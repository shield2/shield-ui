import React from 'react';
import { styles } from './myDefenses.css.js';
import { withStyles, Button, Card, CardContent, Typography, CardActions, Grid, Fab } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { navigate } from '@reach/router';

class MyDefense extends React.Component {

    //const [open, setOpen] = useState(false);


    constructor(props) {
        super(props);

        this.state = {
            open: false,
            registerToDelete: "",
        };
    }

    render() {
        const { classes } = this.props
        const myDefenses = [
            {
                _id: "1",
                courseName: "אפליקציות אינטרנטיות",
                description: "הגנה על פרויקט גמר בקורס נושאים מתקדמים באפליקציות אינטרנטיות ",
                date: "21/2/2020",
                lecturer: "איגור רוכלין"
            },
            {
                _id: "2",
                courseName: "אפליקציות ניידות",
                description: "הגנה על פרויקט גמר בקורס אפליקציות מובייל ל IOS ",
                date: "27/2/2020",
                lecturer: "אליאב מנשה"
            }
        ]


        const handleClickOpen = () => {
            this.setState({ open: true });
        };

        const handleClose = () => {
            this.setState({ open: false });
        };

        return (
            <div className={classes.root}>
                <h1 className={classes.mainTitle}> ההגנות שלי</h1>
                <Grid container spacing={2}>
                    {myDefenses.map(elem => (
                        <Grid item key={myDefenses.indexOf(elem)}>
                            <Card className={classes.card}>
                                <CardContent>
                                    <Typography className={classes.text} gutterBottom variant="h5" component="h2">
                                        {elem.courseName} {elem.date}
                                    </Typography>
                                    <Typography className={classes.text} variant="body2" color="textSecondary" component="p">
                                        מרצה: {elem.lecturer}
                                    </Typography>
                                    <Typography className={classes.text} variant="body2" color="textSecondary" component="p">
                                        {elem.description}
                                    </Typography>
                                </CardContent>
                                <CardActions className={classes.cardButton}>
                                    <Fab className={classes.deleteBtn} size="small" aria-label="delete" onClick={handleClickOpen}>
                                        <DeleteIcon />
                                    </Fab>
                                    <Fab className={classes.editBtn} size="small" aria-label="edit" onClick={() => navigate('/DefenseRegister/EditRegister/' + elem._id)}>
                                        <EditIcon />
                                    </Fab>
                                    <Dialog
                                        className={classes.dialog}
                                        open={this.state.open}
                                        onClose={handleClose}
                                        aria-labelledby="alert-dialog-title"
                                        aria-describedby="alert-dialog-description">
                                        <DialogTitle id="alert-dialog-title">{"אישור מחיקה"}</DialogTitle>
                                        <DialogContent>
                                            <DialogContentText id="alert-dialog-description">
                                                האם אתה בטוח שברצנוך למחוק רישום להגנה זה?
                                            </DialogContentText>
                                        </DialogContent>
                                        <DialogActions>
                                            <Button onClick={handleClose} className={classes.editBtn}>
                                                כן
                                            </Button>
                                            <Button onClick={handleClose} className={classes.deleteBtn}>
                                                לא
                                            </Button>
                                        </DialogActions>
                                    </Dialog>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(MyDefense)