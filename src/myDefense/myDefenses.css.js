export const styles = (theme) => ({
    root: {
        direction: "rtl",
        margin: "5%",
        marginRight: 210,
        flexGrow: 1,
    },
    mainTitle: {
        direction: "rtl",
        textAlign: "right",
        textShadow: "2px 2px #b5c5b9"
    },
    editBtn: {
        backgroundColor: "#00bfa5",
        color: "white",
        margin: 5,
    },
    deleteBtn: {
        backgroundColor: "#d8626d",
        color: "white",
        margin: 5,
    },
    card: {
        maxWidth: 280,
        backgroundColor: "#f5f5f5",
        margin: 10
    },
    text: {
        textAlign: "right",
        direction: "rtl",
    },
    search: {
        textAlign: "right",
        direction: "rtl",
        margin: 15
    },
    iconButton: {
        padding: 10
    },
    cardButton: {
        direction: "ltr"
    },
    dialog: {
        direction: "rtl",
        textAlign: "right"
    }
})