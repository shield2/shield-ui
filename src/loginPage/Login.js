import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useStyles } from './Login.css.js';
import { TextField, CardActions, Card, CardContent } from '@material-ui/core'
import { navigate } from '@reach/router'
import axios from 'axios';

export default function Login(props) {
  const classes = useStyles();

  const [username, setUsername] = useState("");
  const [id, setId] = useState("");

  function validateForm() {
    return username.length > 0 && id.length === 9;
  }

  function handleSubmit(event) {
    const user = {
      id,
      username,
    };
    axios.post("http://localhost:8080/login", user, { withCredentials: true }).then(res => {
      document.cookie = "userAdded=0"
      navigate('/')
    }).catch(err => {
      console.log("error")
    })
  }


  return (
    <div>
      <Form className={classes.loginForm}>
        <Card className={classes.card}>
          <CardContent>
            <Form.Row>
              <TextField className={classes.text}
                label="שם מלא"
                placeholder="הכנס שם מלא"
                value={username}
                onChange={e => setUsername(e.target.value)}
                type="search"
                variant="outlined" />
            </Form.Row>
            <br></br>
            <Form.Row>
              <TextField className={classes.text}
                label="תעודת זהות"
                placeholder="הכנס תעודת זהות"
                value={id}
                onChange={e => setId(e.target.value)}
                type="search"
                variant="outlined" />
            </Form.Row>
          </CardContent>
          <CardActions className={classes.btnSubmit} >
            <br></br>
            <Button variant="primary" disabled={!validateForm()} onClick={() => {
              handleSubmit()
            }
            }>
              התחבר
            </Button>
          </CardActions>
        </Card>
      </Form>
    </div>
  );
}