import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
    card: {
        maxWidth: 250,
        transition: "0.3s",
        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
        "&:hover": {
            boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
        }
    },

    loginForm: {
        marginLeft: '43%',
        marginTop: '17%',
    },

    btnSubmit: {
        marginLeft: '28%',
    },

    text: {
        textAlign: 'right',
        direction: 'rtl',
    },
})






