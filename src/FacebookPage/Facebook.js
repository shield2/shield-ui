import React from 'react';
import { styles } from './Facebook.css.js';
import { withStyles } from "@material-ui/core";
import { FacebookProvider, Page } from 'react-facebook';

class Facebook extends React.Component {

    render() {
        const { classes } = this.props
        return (
            <div className={classes.faceBookDiv}>
                <FacebookProvider appId="2746184032102606">
                    <Page
                        href="https://www.facebook.com/Logic.of.Student" tabs="timeline"
                        width="800"
                        height="900"
                    />
                </FacebookProvider>
            </div>
        );
    }
}

export default withStyles(styles)(Facebook);