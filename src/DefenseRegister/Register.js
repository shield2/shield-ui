import React from 'react';
import { Form, Col } from "react-bootstrap";
import { styles } from './Register.css.js';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import { Fab, Button, Paper, Tooltip, IconButton, withStyles } from '@material-ui/core';
import axios from "axios";
import { navigate } from '@reach/router';

class Register extends React.Component {

  constructor(props) {
    super();

    this.state = {
      defenseId: props.defenseId, // this is a string...
      time: "",

      defense: [],
      students: [],
      hours: [],
      // props
      minStudents: 2,
      maxStudents: 6
    };
  }

  componentDidMount() {
    const students = [];
    for (let index = 0; index < this.state.minStudents; index++) {
      students.push({ id: null })
    }

    const data = {
      defenseId: this.state.defenseId
    };
    axios.post("http://localhost:8080/getDefenseRegisterById", data, { withCredentials: true }).then(res => {
      //var dr = res.data
      this.setState({hours: res.data})
    }).catch(err => {
      console.log("error")
    })

    // const defense = {
    //   courseName: "אפליקציות אינטרנטיות",
    //   date: "21/2/2020",
    //   lecturer: "איגור רוכלין",
    //   minStudents: '2'
    // }

    axios.post("http://localhost:8080/getDefenseById", data, { withCredentials: true }).then(res => {
      //var dr = res.data
      this.setState({defense: res.data})
    }).catch(err => {
      console.log("error")
    })

    this.setState({
      students: students
    });
  }

  handleTimeChange = evt => {
    this.setState({ time: evt.target.value });
  };

  handleStudentIDChange = idx => evt => {
    const newStudents = this.state.students.map((student, sidx) => {
      if (idx !== sidx) return student;
      return { ...student, id: evt.target.value };
    });

    this.setState({ students: newStudents });
  };

  handleSubmit = evt => {
    const { time, students } = this.state;

    if (this.state.time !== "" && this.state.students.length >= 2) {
      alert(`נרשמת להגנה בשעה: ${time} כקבוצה של  ${students.length} סטודנטים`);
    }
    else if (time === "") {
      alert(`חובה לבחור שעה להגנה`);
    }
    else if (students.length < 2) {
      alert("מינימום שני סטודנטים בפרויקט");
    }

    var sid = []
    students.forEach(element => { sid = [...sid, element.id]});
    
    var data = {
      defenseId: this.state.defenseId,
      hour: this.state.time,
      students: sid
    }

    axios.post("http://localhost:8080/register", data, { withCredentials: true }).then(res => {
      navigate('/')
    }).catch(err => {
      console.log("error")
    })
  };

  handleAddStudent = () => {
    this.setState({
      students: this.state.students.concat([{ id: "" }])
    })
  };

  handleRemoveStudent = idx => () => {
    this.setState({
      students: this.state.students.filter((s, sidx) => idx !== sidx)
    });
  };

  render() {
    const { classes } = this.props
    const { hours, defense } = this.state
    // const defense = {
    //   courseName: "אפליקציות אינטרנטיות",
    //   date: "21/2/2020",
    //   lecturer: "איגור רוכלין",
    //   minStudents: '2'
    // }
    //const hours = [{ time: '16:05' }, { time: '16:35' }, { time: '17:55' }, { time: '20:15' }]

    return (
      <div className={classes.root}>
        <div>
          <h1 className={classes.mainTitle}> הרשמה למועד הגנה</h1>
        </div>
        <Paper elevation={3} className={classes.paper}>
          <Form className={classes.text}>
            <Form.Row>
              <Form.Label column sm="2">
                שם הקורס:
                    </Form.Label>
              <Col sm="10">
                <Form.Control plaintext readOnly defaultValue={defense.courseName} />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="2">
                מרצה:
                    </Form.Label>
              <Col sm="10">
                <Form.Control plaintext readOnly defaultValue={defense.lecturer} />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="2">
                תאריך ההגנה:
                    </Form.Label>
              <Col sm="10">
                <Form.Control plaintext readOnly defaultValue={defense.date} />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="2">שעה:</Form.Label>
              <Col sm="10">
                <Form.Control as="select"
                  value={this.state.time} onChange={this.handleTimeChange} className={classes.list}>
                  <option item key="null"> בחר שעה</option>
                  {hours.map(elem => (
                    <option item key={hours.indexOf(elem)}>{elem.hour}</option>
                  ))}
                </Form.Control>
              </Col>
            </Form.Row>
            <br />

            <form onSubmit={this.handleSubmit}>
              <h6>תעודות זהות סטודנטים</h6>
              {this.state.students.map((student, idx) => (
                <div className="student">
                  <input
                    type="text"
                    placeholder={`תעודת זהות סטודנט ${idx + 1}`}
                    value={student.id}
                    onChange={this.handleStudentIDChange(idx)}
                    className={classes.input}
                  />
                  {idx >= this.state.minStudents &&
                    <Tooltip title="מחק סטודנט" aria-label="delete">
                      <IconButton aria-label="delete" onClick={this.handleRemoveStudent(idx)} className={classes.btnDelete}>
                        <DeleteIcon fontSize="small" />
                      </IconButton>
                    </Tooltip>}
                </div>
              ))}

              {this.state.students.length < this.state.maxStudents &&
                <Tooltip title="הוסף סטודנט" aria-label="add">
                  <Fab
                    className={classes.btnAdd}
                    aria-label="add"
                    type="button"
                    onClick={this.handleAddStudent}
                    size="small">
                    <AddIcon />
                  </Fab>
                </Tooltip>
              }
              <br /><br />
              <Button variant="contained" type="submit" className={classes.btnSubmit}>
                הרשם
            </Button>
            </form>
          </Form>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(Register);