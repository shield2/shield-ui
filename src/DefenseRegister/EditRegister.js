import React from 'react';
import { Form, Col } from "react-bootstrap";
import { styles } from './Register.css.js';
import { Paper, withStyles } from '@material-ui/core'


class EditRegister extends React.Component {
    constructor(props) {
        super();
    
        this.state = {
          registerId: props.registerId, // this is a string...
        };
      }
    render() {
        const { classes } = this.props
        const currDefense = {
            courseName: "אפליקציות אינטרנטיות",
            date: "21/2/2020",
            lecturer: "איגור רוכלין",
            maxStudents: 3,
            hour: { time: '16:20' }
        }
        const dates = [{ date: '20/02/20' }, { date: '21/02/20' }]
        const hours = [{ time: '16:05' }, { time: '16:35' }, { time: '17:55' }, { time: '20:15' }]
        const studentsNames = ['מאי גליבטר', 'איש המלבי', 'רון אזואלוס']

        return (
            <div className={classes.root}>
                <div>
                    <h1 className={classes.mainTitle}> עדכון הרשמה למועד הגנה</h1>
                    <h4>{currDefense.courseName + '- ' + currDefense.lecturer} </h4>
                    <h6>{'אתה רשום להגנה בתאריך ' + currDefense.date + ' בשעה ' + currDefense.hour.time}</h6>
                </div>
                <Paper elevation={3} className={classes.paper}>
                    <Form className={classes.text}>
                        <Form.Row>
                            <Form.Label column className={classes.formLabel} sm="1">תאריך:</Form.Label>
                            <Col sm="4">
                                <Form.Control as="select" className={classes.formSelect}>
                                    {dates.map(elem => (
                                        <option item key={dates.indexOf(elem)}>{elem.date}</option>
                                    ))}
                                </Form.Control>
                            </Col>
                        </Form.Row>
                        <br />
                        <Form.Row>
                            <Form.Label column className={classes.formLabel} sm="1">שעה:</Form.Label>
                            <Col sm="4">
                                <Form.Control as="select">
                                    {hours.map(elem => (
                                        <option item key={hours.indexOf(elem)}>{elem.time}</option>
                                    ))}
                                </Form.Control>
                            </Col>
                        </Form.Row>
                        <br />
                        <h5>סטודטנים בקבוצה:</h5>
                        <ul>
                            {studentsNames.map(item => (
                                <li key={item}>{item}</li>
                            ))}
                        </ul>
                        <h6>הוספת סטודנט:</h6>
                        <Form.Row>
                            <Col sm="4">
                                <Form.Control placeholder="תעודת זהות" disabled={studentsNames.length === currDefense.maxStudents} />
                                {studentsNames.length === currDefense.maxStudents &&
                                    <p className={classes.formErr}>שים לב כי מספר חברי הקבוצה הינו מקסימלי</p>}
                            </Col>
                        </Form.Row>

                    </Form>
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(EditRegister);