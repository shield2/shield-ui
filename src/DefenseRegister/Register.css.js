export const styles = (theme) => ({
    root: {
        direction: "rtl",
        margin: "5%",
        marginRight: 210,
        flexGrow: 1,
    },
    text: {
        textAlign: "right",
        direction: "rtl",
        margin: "5%",
    },
    btnSubmit: {
        marginRight: "45%",
        marginBottom: "3%"
    },
    btnDelete: {
        marginRight: "2%",
    },
    input: {
        margin: 5,
    },
    btnAdd: {
        backgroundColor: "#4d94ff",
        color: "white",
        margin: 5
    },
    paper: {
        maxHeight: 1400,
        maxWidth: 700,
        marginTop: "1%",
        marginRight: "15%"
    },
    list: {
        maxWidth: 200,
    },
    mainTitle: {
        textAlign: "right",
        direction: "rtl",
        textShadow: "2px 2px #b5c5b9"
    },
    formErr: {
        textAlign: "right",
        direction: "rtl",
        fontSize: "12px",
        color: "red"
    },
    formLabel: {
        textAlign: "left",
        marginTop: "1%",
    },
    formSelect: {
        marginTop: "5%",
    }
})