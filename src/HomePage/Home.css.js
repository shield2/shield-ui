export const styles = (theme) => ({
  mainTitle: {
    direction: "rtl",
    textAlign: "right",
    textShadow: "2px 2px #b5c5b9"
  },
  root: {
    direction: "rtl",
    margin: "5%",
    marginRight: 210,
    flexGrow: 1,
  },
  card: {
    maxWidth: 280,
    backgroundColor: "#f5f5f5",
    margin: 10
  },
  text: {
    textAlign: "right",
    direction: "rtl",
  },
  search: {
    textAlign: "right",
    direction: "rtl",
    margin: 15
  },
  iconButton: {
    padding: 10
  },
  addBtn: {
    // 80bfff
    backgroundColor: "#80bfff",
    color: "white",
    margin: 5,
  },
  cardButton: {
    direction: "ltr"
  }
})