import React from 'react';
import { styles } from './Home.css.js';
import { withStyles, Card, CardContent, Typography, IconButton, CardActions, Grid, TextField, Fab, Tooltip } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search';
import AssignmentOutlinedIcon from '@material-ui/icons/AssignmentOutlined';
import { Form } from 'react-bootstrap';
import { navigate } from '@reach/router';
import axios from 'axios';

class Home extends React.Component {
  constructor() {
    super();
    //const defenses = this.getAllDefenses()
    this.state = {

      allDefenses: [],
      lecturer: '',
      course: '',
      semester: '',
      
    };
  }

  componentDidMount()
  {
    axios.get("http://localhost:8080/allDefenses", { withCredentials: true }).then(res => {
      var defenses = res.data;
      defenses.forEach (function (item, index){
        item.date = new Date(item.date);
      })
      this.setState({ allDefenses: defenses})
    }).catch(err => {
      console.log("get courses error")
    })
  }

  onCourseChangeHandler = (e) => {
    this.setState({
      course: e.target.value
    })
  }

  onLecturerChangeHandler = (e) => {
    this.setState({
      lecturer: e.target.value
    })
  }

  onSemeaterChangeHandler = (e) => {
    this.setState({
      semester: e.target.value
    })
  }

  handleSearch = () => {
    //alert("bla")
    var data = {
      course: this.state.course,
      year: this.state.lecturer,
      semester: this.state.semester
    };
    axios.post("http://localhost:8080/searchDefenses", data, { withCredentials: true }).then(res => {
      const defenses = res.data;
      console.log(defenses)
      if(defenses)
      {
        this.setState({allDefenses: defenses})
      }
      
    }).catch(err => {
      console.log("error")
    })
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <h1 className={classes.mainTitle}>דף הבית</h1>
        <Form.Row>
          <TextField type="input" className={classes.search} id="standard-basic" label="שם הקורס" onChange={this.onCourseChangeHandler} />
          <TextField type="input" className={classes.search} id="standard-basic" label="שם המרצה" onChange={this.onLecturerChangeHandler} />
          <TextField type="input" className={classes.search} id="standard-basic" label="סמסטר" onChange={this.onSemeaterChangeHandler} />

          <IconButton type="submit" className={classes.iconButton} aria-label="search">
            <SearchIcon />
          </IconButton>
        </Form.Row>
        <br />
        <Grid container spacing={2}>
          {this.state.allDefenses.filter(defense => {
            return (defense.courseName.includes(this.state.course) &&
                    defense.lecturer.includes(this.state.lecturer) && 
                    defense.semester.includes(this.state.semester))
          }).map((elem, index) => (
            <Grid item key={index}>
              <Card className={classes.card}>
                <CardContent>
                  <Typography className={classes.text} gutterBottom variant="h5" component="h2">
                    {elem.courseName} {elem.date.getDate()+'/' + (elem.date.getMonth()+1) + '/'+elem.date.getFullYear()}
                  </Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p">
                    סמסטר {elem.semester}
                  </Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p">
                    מרצה: {elem.lecturer}
                  </Typography>
                  <Typography className={classes.text} variant="body1" color="textSecondary" component="p">
                    {elem.description}
                  </Typography>
                </CardContent>
                <CardActions className={classes.cardButton}>
                  <Tooltip title="הרשמה" aria-label="add">
                    <Fab className={classes.addBtn} size="small" aria-label="add" onClick={() => navigate('/DefenseRegister/Register/' + elem._id)}>
                      <AssignmentOutlinedIcon />
                    </Fab>
                  </Tooltip>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Home)