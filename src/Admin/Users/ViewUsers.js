import React from "react";
import { styles } from "../Courses/Style.css.js";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from 'axios';
import { ReactBingmaps } from "react-bingmaps";
import {
  Fab,
  Button,
  Tooltip,
  TextField,
  Checkbox,
  withStyles
} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import { Form, Col } from "react-bootstrap";

class ViewUsers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      allUsers: [],

      userAdded: "", 

      // New user details
      addFirstName: "",
      addLastName: "",
      addId: "",
      addIsAdmin: false,
      addCity: "",
      
      // Details to edit
      editFirstName: "",
      editLastName: "",
      editId: "",
      editIsAdmin: false,
      editCity: "",

      newUser: false, // New user dialog open state
      open: false, // Delete user dialog open state
      editUser: false, // Edit user dialog open state
      mapOpen: false, // Map dialog open state
      
      currId: "", // User id to delete
      currCity: "", // City to present in the map

      // Map parameters
      boundary: {
        search: "",
        option: {
          entityType: "PopulatedPlace"
        },
        polygonStyle: {
          fillColor: "rgba(161,224,255,0.4)",
          strokeColor: "#a495b2",
          strokeThickness: 2
        }
      }
    };
  }

  componentDidMount()
  {
    this.getUsers();
  }

  getUsers()
  {
    axios.get("http://localhost:8080/allUsers", { withCredentials: true }).then(res => {
      this.setState({ allUsers: res.data})
    }).catch(err => {
      console.log("get courses error")
    })
    console.log("get users")
  }

  render() {
    const { classes } = this.props;

    const handleClickOpen = idToDelete => {
      this.setState({ open: true });
      this.setState({currId: idToDelete});
        };

    const handleClose = () => {
      this.setState({ open: false });
    };

    const handleClickEditOpen = uId => {
      this.setState({ editUser: true });
      var userToEdit = this.state.allUsers.find(u => u.id === uId)
      this.setState({
        editFirstName: userToEdit.firstName,
        editLastName: userToEdit.lastName,
        editId: userToEdit.id,
        editIsAdmin: userToEdit.isAdmin,
        editCity: userToEdit.city
      })
    };

    const handleMapClickOpen = city => {
      this.setState({ mapOpen: true });
      this.setState({
        boundary: {
          search: city,
          polygonStyle: {
            fillColor: "rgba(161,224,255,0.4)",
            strokeColor: "#a495b2",
            strokeThickness: 2
          },
          option: {
            entityType: "PopulatedPlace"
          }
        }
      });
    };

    const handleEditClose = () => {
      this.setState({ editUser: false });
    };

    const handleMapClose = () => {
      this.setState({ mapOpen: false });
    };

    const handleNewUserOpen = () => {
      this.setState({ newUser: true });
    };

    const handleNewUserClose = () => {
      this.setState({ newUser: false });
    };

    const handleChanged = e => {
      this.setState({[e.target.name]: e.target.value})
    };

    const handleChecked = e => {
      this.setState({[e.target.name]: e.target.checked})
    };

    const readCookie = name=> {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1,c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return null;
  }

    const addUser = () => {
      var data = {
        firstName: this.state.addFirstName,
        lastName: this.state.addLastName,
        id: this.state.addId,
        isAdmin: (this.state.addIsAdmin === "true" ? true : false),
        city: this.state.addCity
      };

      console.log(document.cookie)
      var userAdded = readCookie('userAdded') + 1;
      console.log(userAdded)
      document.cookie = 'userAdded=' + userAdded
      this.setState({userAdded:userAdded}) 


      var errors = []
      if(data.firstName === "")
        errors.push("לא הוזן שם פרטי")
      if(data.lastName === "")
        errors.push("לא הוזן שם משפחה")
      if(!data.id.match('[0-9]{9}') )
        errors.push("תעודת זהות לא תקינה")
      if(this.state.allUsers.find(x => x.id === data.id) !== undefined)
        errors.push("תעודת זהות קיימת במערכת")
      if(data.city === "")
        errors.push("לא הוזן מקום מגורים")
      

      if (errors.length === 0) {
        axios.post("http://localhost:8080/addUser", data, { withCredentials: true }).then(res => {
          console.log(res)
          console.log("success")
          let get = this.getUsers.bind(this)
          get();
        }).catch(err => {
          console.log("error")
        })
        handleNewUserClose()
      } else {
        alert(errors.join('\n'))
      }

      this.setState({addFirstName: "",
                      addLastName: "",
                      addId: "",
                      addIsAdmin: false,
                      addCity: ""})
    }

    const updateUser = () => {
      var data = {
        firstName: this.state.editFirstName,
        lastName: this.state.editLastName,
        id: this.state.editId,
        isAdmin: (this.state.editIsAdmin === "true" ? true : false),
        city: this.state.editCity
      };

      var errors = []
      if(data.firstName === "")
        errors.push("First name is empty")
      if(data.lastName === "")
        errors.push("Last name is empty")
      if(data.city === "")
        errors.push("City is empty")

      if (errors.length === 0) {
        axios.post("http://localhost:8080/updateUser", data, { withCredentials: true }).then(res => {
          console.log("success")
          let get = this.getUsers.bind(this)
          get(); 
          }).catch(err => {
          console.log("error")
        })
        handleEditClose()
      } else {
        alert(errors.join('\n'))
      }
   }

    const deleteUser = () => {
      var data = {
        id: this.state.currId
      };

      axios.post("http://localhost:8080/deleteUser", data, { withCredentials: true }).then(res => {
        console.log("success")
        let get = this.getUsers.bind(this)
        get();
      }).catch(err => {
        console.log("error")
      })

      
      handleClose()
    }

    return (
      <div className={classes.root}>
        <div>
          <h1 className={classes.mainTitle}> משתמשי המערכת</h1>
          <Tooltip title="הוספת משתמש" aria-label="add">
            <Fab
              size="small"
              className={classes.btnAdd}
              aria-label="add"
              onClick={handleNewUserOpen}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
          {this.state.userAdded > 1 &&
          <h5>  ברכות! הוספת יותר משני יוזרים בגרסא הבאה נאפשר עבורך העלאת משתמשים בExcel</h5>
          }
          <Dialog
            open={this.state.editUser}
            onClose={handleEditClose}
            aria-labelledby="form-dialog-title"
            className={classes.dialog}
          >
            <DialogTitle id="form-dialog-title">עריכת משתמש קיים</DialogTitle>
            <DialogContent>
              <Form.Row>
                <Form.Label column sm="3">
                  שם פרטי :
                </Form.Label>
                <TextField
                  autoFocus
                  margin="dense"
                  type="text"
                  name="editFirstName"
                  defaultValue={this.state.editFirstName}
                  onChange={handleChanged}
                />
              </Form.Row>
              <Form.Row>
                <Form.Label column sm="3">
                  שם משפחה :
                </Form.Label>
                <TextField
                  autoFocus
                  margin="dense"
                  type="text"
                  name="editLastName"
                  value={this.state.editLastName}
                  onChange={handleChanged}
                />
              </Form.Row>
              <Form.Label column sm="4">
                תעודת זהות:
              </Form.Label>
              <TextField
                autoFocus
                disabled
                margin="dense"
                type="text"
                name="editId"
                value={this.state.editId}
                onChange={handleChanged}
              />
              <Form.Label column sm="4">
                האם מרצה?
              </Form.Label>
              <Checkbox
                onChange={handleChecked}
                value={this.state.editIsAdmin}
                name="editIsAdmin"
                color="primary"
                inputProps={{ "aria-label": "secondary checkbox" }}
              />
              <Col sm="5">
                <Form.Control 
                  as="select" 
                  name="editCity"
                  value={this.state.editCity}
                  onChange={handleChanged}>
                  <option value="">בחר עיר מגורים</option>
                  <option value="אשדוד">אשדוד</option>
                  <option value="סינגפור">סינגפור</option>
                  <option value="תל אביב">תל אביב</option>
                  <option value="חיפה">חיפה</option>
                  <option value="אילת">אילת</option>
                  <option value="מנהטן">מנהטן</option>
                  <option value="הונג קונג">הונג קונג</option>
                </Form.Control>
              </Col>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleEditClose} color="primary">
                ביטול
              </Button>
              <Button onClick={updateUser} color="primary">
                שמירה
              </Button>
            </DialogActions>
          </Dialog>

          <Dialog
            open={this.state.newUser}
            onClose={handleNewUserClose}
            aria-labelledby="form-dialog-title"
            className={classes.dialog}
          >
            <DialogTitle id="form-dialog-title">הוספת משתמש חדש</DialogTitle>
            <DialogContent>
              <Form.Row>
                <Form.Label column sm="3">
                  שם פרטי :
                </Form.Label>
                <TextField
                  required
                  autoFocus
                  margin="dense"
                  type="text"
                  name="addFirstName"
                  value={this.state.addFirstName}
                  onChange={handleChanged}
                />
              </Form.Row>
              <Form.Row>
                <Form.Label column sm="3">
                  שם משפחה :
                </Form.Label>
                <TextField
                  autoFocus
                  margin="dense"
                  type="text"
                  name="addLastName"
                  value={this.state.addLastName}
                  onChange={handleChanged}
                />
              </Form.Row>
              <Form.Label column sm="4">
                תעודת זהות :
              </Form.Label>
              <TextField
                autoFocus
                margin="dense"
                type="text"
                name="addId"
                value={this.state.addId}
                onChange={handleChanged}
              />
              <Form.Label column sm="4">
                האם מרצה?
              </Form.Label>
              <Checkbox
                onChange={handleChecked}
                name="addIsAdmin"
                value={this.state.addIsAdmin}
                color="primary"
                inputProps={{ "aria-label": "secondary checkbox" }}
              />

              <Col sm="5">
                <Form.Control 
                  as="select" 
                  name="addCity"
                  value={this.state.addCity}
                  onChange={handleChanged}>
                  <option value="">בחר עיר מגורים</option>
                  <option value="אשדוד">אשדוד</option>
                  <option value="סינגפור">סינגפור</option>
                  <option value="תל אביב">תל אביב</option>
                  <option value="חיפה">חיפה</option>
                  <option value="אילת">אילת</option>
                  <option value="מנהטן">מנהטן</option>
                  <option value="הונג קונג">הונג קונג</option>
                </Form.Control>
              </Col>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleNewUserClose} color="primary">
                ביטול
              </Button>
              <Button onClick={addUser} color="primary">
                יצירת משתמש
              </Button>
            </DialogActions>
          </Dialog>
        </div>

        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow background="#eeeeee">
                <TableCell align="right">מספר זהות</TableCell>
                <TableCell align="right">שם פרטי</TableCell>
                <TableCell align="right">שם משפחה</TableCell>
                <TableCell align="right">האם מנהל?</TableCell>
                <TableCell align="right">עריכה</TableCell>
                <TableCell align="right">מחיקה</TableCell>
                <TableCell align="right">מגורים</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.allUsers.map(u => (
                <TableRow key={u.id}>
                  <TableCell align="right">{u.id}</TableCell>
                  <TableCell align="right">{u.firstName}</TableCell>
                  <TableCell align="right">{u.lastName}</TableCell>
                  <TableCell align="right">{u.isAdmin ? "כן" : "לא"}</TableCell>
                  <TableCell align="right">
                    <Fab
                      size="small"
                      className={classes.editBtn}
                      aria-label="edit"
                      onClick={() => handleClickEditOpen(u.id)}
                    >
                      <EditIcon />
                    </Fab>
                  </TableCell>
                  <TableCell align="right">
                    <Fab
                      size="small"
                      className={classes.deleteBtn}
                      aria-label="delete"
                      onClick={() => handleClickOpen(u.id)}
                    >
                      <DeleteIcon />
                    </Fab>
                  </TableCell>
                  <TableCell align="right">
                    <Fab
                      className={classes.locBtn}
                      variant="extended"
                      size="small"
                      color="primary"
                      aria-label="add"
                      onClick={() => handleMapClickOpen(u.city)}
                    >
                      <LocationOnIcon />
                      {u.city}
                    </Fab>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Dialog
          className={classes.dialog}
          open={this.state.open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"אישור מחיקה"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              האם אתה בטוח שברצנוך למחוק משתמש זה?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={deleteUser} className={classes.editBtn}>
              כן
            </Button>
            <Button onClick={handleClose} className={classes.deleteBtn}>
              לא
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          fullScreen
          className={classes.dialog}
          open={this.state.mapOpen}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"מפה"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              העיר הנוכחית: {this.state.currCity}
              <ReactBingmaps
                bingmapKey="Ajn_vRRk9fpS82EDItS1JGK_4WFGXrqXneTU5o2aJoDQ2YJ2Iu5wGTE-g9azbfy2"
                mapTypeId={"road"}
                className={classes.map}
                boundary={this.state.boundary}
                center={[13.0827, 80.2707]}
                zoom={10}
              ></ReactBingmaps>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleMapClose} className={classes.deleteBtn}>
              סגור
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(ViewUsers);
