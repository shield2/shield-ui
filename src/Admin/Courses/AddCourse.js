import React from "react";
import { Form, Col } from "react-bootstrap";
import { styles } from "./Style.css.js";
import { navigate } from "@reach/router";
import { Button, Paper, Input, Grid, withStyles } from "@material-ui/core";
import axios from "axios";

class AddCourse extends React.Component {
  constructor() {
    super();
    this.state = {
      courseName: "",
      lecturer: "",
      lecturerID: "",
      year: "",
      semester: "",
      minStudents: "",
      maxStudents: "",
      studentsNum: "",
      about: ""
    };
  }

  handleCourseChange = evt => {
    this.setState({ courseName: evt.target.value });
  };

  handleIDChange = evt => {
    this.setState({ lecturerID: evt.target.value });
  };

  handleMinChange = evt => {
    this.setState({ minStudents: evt.target.value });
  };

  handleMaxChange = evt => {
    this.setState({ maxStudents: evt.target.value });
  };

  handleAboutChange = evt => {
    this.setState({ about: evt.target.value });
  };

  handleYearChange = evt => {
    this.setState({ year: evt.target.value });
  };

  handleSemesterChange = evt => {
    this.setState({ semester: evt.target.value });
  };

  handleLecturerChange = evt => {
    this.setState({ lecturer: evt.target.value });
  };

  handleBlur = value => {
    if (value < 0) {
      this.setState({ studentsNum: 0 });
    } else if (value > 120) {
      this.setState({ studentsNum: 120 });
    }
  };

  handleStudentsNumChange = event => {
    this.setState({
      StudentsNum: event.target.value === "" ? "" : Number(event.target.value)
    });
  };

  addCourse = () => {
    var data = {
      name: this.state.courseName,
      lecturerID: this.state.lecturerID,
      lecturer: this.state.lecturer,
      semester: this.state.semester,
      year: this.state.year,
      minStudents: this.state.minStudents,
      maxStudents: this.state.maxStudents,
      numOfStudents: this.state.studentsNum,
      description: this.state.about
    };

    axios.post("http://localhost:8080/addCourse", data, { withCredentials: true }).then(res => {
        console.log("success");
        navigate('/HomeAdmin')
      }).catch(err => {
        console.log("error")
      })
  }

  render() {
    const { classes } = this.props;
    const value = this.StudentsNum;

    return (
      <div className={classes.root}>
        <div className={classes.mainTitle}>
          <h1> קורס חדש</h1>
        </div>
        <Paper elevation={3} className={classes.paper}>
          <Form className={classes.Papertext}>
            <Form.Row>
              <Form.Label column sm="4">
                שם הקורס :
              </Form.Label>
              <Col sm="20">
                <Input
                  placeholder="שם הקורס"
                  onChange={this.handleCourseChange}
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                תעודת זהות מרצה :
              </Form.Label>
              <Col sm="20">
                <Input
                  placeholder="תעודת זהות מרצה"
                  onChange={this.handleIDChange}
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                שם מרצה :
              </Form.Label>
              <Col sm="20">
                <Input
                  placeholder="שם המרצה"
                  onChange={this.handleLecturerChange}
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                שנה :
              </Form.Label>
              <Col sm="20">
                <Input
                  placeholder="שנת לימוד"
                  onChange={this.handleYearChange}
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                סמסטר:
              </Form.Label>
              <Col sm="20">
                <Form.Control as="select" onChange={this.handleSemesterChange}>
                  <option> </option>
                  <option>א</option>
                  <option>ב</option>
                  <option>קיץ</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                מינימום סטודנטים בקבוצה:
              </Form.Label>
              <Col sm="20">
                <Form.Control as="select" onChange={this.handleMinChange}>
                  <option> </option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                מקסימום סטודנטים בקבוצה:
              </Form.Label>
              <Col sm="20">
                <Form.Control as="select" onChange={this.handleMaxChange}>
                  <option> </option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                מספר סטודנטים בקורס:
              </Form.Label>
              <Col sm="20">
                <Grid item>
                  <Input
                    value={value}
                    margin="dense"
                    onChange={this.handleStudentsNumChange}
                    onBlur={this.handleBlur(value)}
                    inputProps={{
                      step: 1,
                      min: 0,
                      max: 120,
                      type: "number",
                      "aria-labelledby": "input-slider"
                    }}
                  />
                </Grid>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                תיאור :
              </Form.Label>
              <Col>
                <Input
                  fullWidth
                  placeholder="תיאור"
                  onChange={this.handleAboutChange}
                />
              </Col>
            </Form.Row>

            <br />
            <Button
              variant="contained"
              type="submit"
              className={classes.btnSubmit}
              onClick={this.addCourse}
            >
              יצירת קורס
            </Button>
          </Form>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(AddCourse);
