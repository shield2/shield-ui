import React from "react";
import { styles } from "./Style.css.js";
import { withStyles, Button, Card, CardContent, Typography, CardActions, Grid, TextField, IconButton, Fab, Tooltip, Menu, MenuItem}
from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import BallotIcon from '@material-ui/icons/Ballot';
import AddToPhotosIcon from '@material-ui/icons/AddToPhotos';
import SearchIcon from "@material-ui/icons/Search";
import { Form } from "react-bootstrap";
import { navigate } from "@reach/router";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import axios from 'axios';

class AdminHome extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      allCourses: [],
      //open: false,
      //anchorEl: null,
      currCourse: "",
      name: "", 
      year: "",
      semester: "",
    };
  }

  componentDidMount()
  {
    axios.get("http://localhost:8080/allCourses", { withCredentials: true }).then(res => {
      const courses = res.data;
      this.setState({ allCourses: courses});
    }).catch(err => {
      console.log("get courses error")
    })
  }

  render() {
    const { classes } = this.props
    const open = Boolean(this.state.anchorEl);

    // const handleListClick = event => {
    //   this.setState({ anchorEl: event.currentTarget });
    // };

    // const handleListClose = () => {
    //   this.setState({ anchorEl: null });
    // };

    const handleClickOpen = CourseToDelete => {
      this.setState({ open: true });
      this.setState({currCourse: CourseToDelete});
    };

    const handleClose = () => {
      this.setState({ open: false });
    };

    const deleteCourse = () => {
      var data = {
        id: this.state.currCourse
      };

      console.log(this.state.currCourse)

      axios.post("http://localhost:8080/deleteCourse", data, { withCredentials: true }).then(res => {
        console.log("success")
      }).catch(err => {
        console.log("error")
      })

      handleClose()
    }
    
    const handleCourseChange = e => {
      this.setState({
        name: e.target.value
      })
    }

    const handleYearChange = e => {
      this.setState({
        year: e.target.value
      })
    }

    const handleSemesterChange = e => {
      this.setState({
        semester: e.target.value
      })
    }

    const handleSearch = () => {
      
      axios.get(`http://localhost:8080/searchCourses?name=${this.state.name}&year=${this.state.year}&semester=${this.state.semester}`).then(res => {
        const courses = res.data;
        console.log(courses)
        if(courses)
        {
          this.setState({allCourses: courses})
        }
        
      }).catch(err => {
        console.log("error")
      })
    }
    return (
      <div className={classes.root}>
        <h1 className={classes.mainTitle}> דף מנהל</h1>
        <Form.Row>
          <TextField className={classes.search} label="שם הקורס" onChange={handleCourseChange}/>
          <TextField className={classes.search} label="שנה" onChange={handleYearChange}/>
          <TextField className={classes.search} label="סמסטר" onChange={handleSemesterChange}/>
          <IconButton
            type="submit"
            className={classes.iconButton}
            aria-label="search"
            onClick={handleSearch}
          >
            <SearchIcon />
          </IconButton>
        </Form.Row>
        <Tooltip title="קורס חדש" aria-label="add">
          <Fab
            className={classes.btnAdd}
            aria-label="add"
            type="button"
            size="small"
            onClick={() => {
              navigate("/Courses/AddCourse");
            }}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
        <Grid container spacing={2}>
          {this.state.allCourses.map(elem => (
            <Grid key={this.state.allCourses.indexOf(elem)}>
              <Card className={classes.card}>
                <CardContent>
                  <div>
                    {/* <IconButton
                      aria-label="more"
                      aria-controls="long-menu"
                      aria-haspopup="true"
                      onClick={handleListClick}
                    >
                      <MoreVertIcon />
                    </IconButton>
                    <Menu
                      id="long-menu"
                      anchorEl={this.state.anchorEl}
                      keepMounted
                      open={open}
                      onClose={handleListClose}
                      className = {classes.menu}
                      PaperProps={{
                        style: {
                          maxHeight: 48 * 4.5,
                          width: 200,
                        }
                      }}
                    >
                      <MenuItem onClick={() => { navigate("/Defenses/ShowDefenses"); }}>
                        צפייה בהגנות הקורס
                      </MenuItem>
                      <MenuItem onClick={() => { navigate("/Defenses/AddDefense/"+ elem._id); }}>
                        הוספת הגנה חדשה
                      </MenuItem>
                    </Menu> */}
                  </div>
                  
                  <Typography
                    className={classes.text}
                    gutterBottom
                    variant="h5"
                    component="h2"
                  >
                    {elem.name}
                  </Typography>
                  <Typography
                    className={classes.text}
                    variant="h6"
                    color="textSecondary"
                    component="p"
                  >
                   {elem.year + " " + elem.semester}
                  </Typography>
                  <Typography
                    className={classes.text}
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    מרצה: {elem.lecturer ?? '...'}
                  </Typography>
                </CardContent>
                <CardActions className={classes.cardButton}>
                  <Fab
                    className={classes.deleteBtn}
                    size="small"
                    aria-label="delete"
                    onClick={() => handleClickOpen(elem._id)}
                  >
                    <DeleteIcon />
                  </Fab>
                  <Fab
                    className={classes.editBtn}
                    size="small"
                    aria-label="edit"
                    onClick={() => navigate(`/Courses/EditCourse/${elem._id}`)}
                  >
                    <EditIcon />
                  </Fab>
                  <Fab
                    className={classes.btAddNewDefense}
                    size="small"
                    aria-label="AddDefense"
                    onClick={() => navigate(`/Defenses/AddDefense/${elem._id}`)}
                  >
                    <AddToPhotosIcon />
                  </Fab>
                  <Fab
                    className={classes.btnList}
                    size="small"
                    aria-label="PresentDefenses"
                    onClick={() => navigate(`/Defenses/ShowDefenses/${elem._id}`)}
                  >
                    <BallotIcon />
                  </Fab>
                  {/* <Button  onClick={() => navigate(`/Defenses/AddDefense/${elem._id}`)}>הוספת הגנה</Button> */}
                  {/* <Button onClick={() => navigate(`/Defenses/ShowDefenses/${elem._id}`)}>הצגת הגנות</Button> */}
                  <Dialog
                    className={classes.dialog}
                    open={this.state.open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                  >
                    <DialogTitle id="alert-dialog-title">
                      {"אישור מחיקה"}
                    </DialogTitle>
                    <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                        האם אתה בטוח שברצונך למחוק קורס זה ?
                      </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={deleteCourse} className={classes.editBtn}>
                        כן
                      </Button>
                      <Button onClick={handleClose} className={classes.deleteBtn}>
                        לא
                      </Button>
                    </DialogActions>
                  </Dialog>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(AdminHome)
