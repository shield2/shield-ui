export const styles = () => ({
    root: {
        direction: "rtl",
        margin: "5%",
        marginRight: 210,
        flexGrow: 1,
        textAlign: "right"
    },

    mainTitle: {
        direction: "rtl",
        textAlign: "right",
        textShadow: "2px 2px #b5c5b9",
        marginBottom: "30px"
    },

    editBtn: {
        backgroundColor: "#00bfa5",
        color: "white",
        margin: 5,
    },

    deleteBtn: {
        backgroundColor: "#d8626d",
        color: "white",
        margin: 5,
    },
    card: {
        width: 330,
        height: 240,
        backgroundColor: "#f5f5f5",
        margin: 10
    },

    defenseCard: {
        width: 200,
        height: 150,
        backgroundColor: "#f5f5f5",
        margin: 10
    },

    Papertext: {
        textAlign: "right",
        direction: "rtl",
        margin: '5%'
    },

    text: {
        textAlign: "right",
        direction: "rtl",
        marginRight: "15%"
    },

    search: {
        textAlign: "right",
        direction: "rtl",
        margin: 15
    },

    iconButton: {
        padding: 10
    },

    cardButton: {
        direction: "ltr"
    },

    //#dce775
    btnList: {
        backgroundColor: "#607d8b",
        color: "white",
        marginLeft: 1200,
        marginTop: 20,
        marginBottom: 20
    },

    btAddNewDefense: {
        backgroundColor: "#b0bec5",
        color: "white",
        marginLeft: 1200,
        marginTop: 20,
        marginBottom: 20
    },

    btnAdd: {
        backgroundColor: "#4d94ff",
        color: "white",
        marginLeft: 1200,
        marginTop: 20,
        marginBottom: 20
    },

    locBtn: {
        backgroundColor: "#f8f9fa",
        color: "black",
    },

    btnSubmit: {
        marginRight: "45%",
        marginBottom: "3%"
    },

    paper: {
        maxHeight: 1400,
        maxWidth: 750,
        marginTop: "1%",
        marginRight: "15%"
    },

    map: {
        height: 400,
        width: 600,
        marginTop: "1%",
        marginRight: "15%",
        position: 'absolute !important',
    },

    dialog: {
        direction: "rtl",
        textAlign: "right",
        position: "relative",
        width: "60% !important",
        height: "80% !important",
        top: "10% !important",
        left: "20% !important"
    },

    menu: {
        direction: 'rtl',
        marginLeft: "-13%",
    },

    thead: {
        fontWeight: "1000 !important"
    },

    diagramBtn: {
        backgroundColor: "#90caf9",
        color: "black",
        margin: "10px"
    },
})


// export const styles: {
//     root: root,
//     card: card,
//     text: text,
//     iconButton: iconButton,
//     search: search,
//     mainTitle: mainTitle,
//     editBtn: editBtn,
//     deleteBtn: deleteBtn,
//     cardButton: cardButton,
//     btnAdd: btnAdd,
//     btnSubmit: btnSubmit,
//     paper: paper,
//     Papertext: Papertext,
//     dialog: dialog,
//     menu: menu,
//     map: map,
//     defenseCard: defenseCard,
//     locBtn: locBtn,
// }