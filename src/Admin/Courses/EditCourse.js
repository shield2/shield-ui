import React from 'react';
import { Form, Col } from "react-bootstrap";
import { styles } from './Style.css.js';
import { Button, Paper, Input, Grid, withStyles } from '@material-ui/core'
import axios from 'axios';
import { navigate } from "@reach/router";


class EditCourse extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      courseName: "",
      lecturerID: "",
      lecturer: "",
      year: "",
      semester: "",
      minStudents: "",
      maxStudents: "",
      studentsNum: "",
      about: "",
    };
  }

  componentDidMount() {

    console.log(this.props.courseId);

    axios.get("http://localhost:8080/getCourseByID?id=" + this.props.courseId).then(res => {
        const course = res.data;
        this.setState({ courseName: course.name,
                        lecturerID: course.lecturerID,
                        lecturer: course.lecturer,
                        year: course.year,
                        semester: course.semester,
                        minStudents: course.minStudents,
                        maxStudents: course.maxStudents, 
                        studentsNum: course.numOfStudents,
                        about: course.description,});
        console.log(course)      
      }).catch(err => {
        console.log("error")
      })

  }

  handleSubmit = evt => {

    alert(`עדכון קורס`);

  };

  handleCourseChange = evt => {
    this.setState({ courseName: evt.target.value })
  };

  handleLecturerChange = evt => {
    this.setState({ lecturer: evt.target.value })
  };

  handleIDChange = evt => {
    this.setState({ id: evt.target.value })
  };

  handleMinChange = evt => {
    this.setState({ minStudents: evt.target.value })
  };

  handleMaxChange = evt => {
    this.setState({ maxStudents: evt.target.value })
  };

  handleSemesterChange = evt => {
    this.setState({ semester: evt.target.value })
  };

  handleYearChange = evt => {
    this.setState({ year: evt.target.value })
  };

  handleBlur = (value) => {
    if (value < 0) {
      this.setState({ studentsNum: 0 });
    }
    else if (value > 120) {
      this.setState({ studentsNum: 120 });
    }
  };


  handleAboutChange = event => {
    this.setState({ about: event.target.value })
  };


  handleStudentsNumChange = event => {
    this.setState({ StudentsNum: event.target.value === '' ? '' : Number(event.target.value) });
  };


  updateCourse = () => {
    var data = {name: this.state.courseName,
                lecturerID: this.state.lecturerID,
                lecturer: this.state.lecturer,
                year: this.state.year,
                semester: this.state.semester,
                minStudents: this.state.minStudents,
                maxStudents: this.state.maxStudents, 
                numOfStudents: this.state.studentsNum,
                description: this.state.about,
                _id: this.props.courseId
    };

    var errors = []
    if(data.name === "")
      errors.push("course name is empty")
    if(data.lecturerID === "")
      errors.push("id is empty")
      if(data.lecturer === "")
      errors.push("lecturer name is empty")
    if(data.year === "")
      errors.push("year is empty")
    if(data.numOfStudents === "")
      errors.push("number of students is empty")
    if(data.maxStudents < data.minStudents)
      errors.push("max students lower than minimum students")

    if (errors.length === 0) {
      axios.post("http://localhost:8080/updateCourse", data).then(res => {
        console.log("success")
        navigate('/AdminHome')
        }).catch(err => {
        console.log("error")
      })

    } else {
      alert(errors.join('\n'))
    }
 }


  render() {
    const { classes } = this.props

    return (
      <div className={classes.root}>
        <div className={classes.mainTitle}>
          <h1> עדכון פרטי קורס</h1>
        </div>
        <Paper elevation={3} className={classes.paper}>
          <Form className={classes.Papertext}>
            <Form.Row>
              <Form.Label column sm="4">
                שם הקורס :
                </Form.Label>
              <Col sm="20">
                <Input value={this.state.courseName} onChange={this.handleCourseChange} />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                תעודת זהות מרצה :
                </Form.Label>
              <Col sm="20">
                <Input value={this.state.lecturerID} onChange={this.handleIDChange} />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                שם מרצה :
                </Form.Label>
              <Col sm="20">
                <Input value={this.state.lecturer} onChange={this.handleLecturerChange} />
              </Col>
            </Form.Row>
            
            <Form.Row>
              <Form.Label column sm="4">
                שנה :
              </Form.Label>
              <Col sm="20">
                <Input
                  value={this.state.year}
                  onChange={this.handleYearChange}
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                סמסטר:
              </Form.Label>
              <Col sm="20">
                <Form.Control as="select" value= {this.state.semester} onChange={this.handleSemesterChange}>
                  <option>א</option>
                  <option>ב</option>
                  <option>קיץ</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">מינימום סטודנטים בקבוצה:</Form.Label>
              <Col sm="20">
                <Form.Control as="select" value={this.state.minStudents} onChange={this.handleMinChange}>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">מקסימום סטודנטים בקבוצה:</Form.Label>
              <Col sm="20">
                <Form.Control as="select" value={this.state.maxStudents} onChange={this.handleMaxChange}>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                </Form.Control>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">מספר סטודנטים בקורס:</Form.Label>
              <Col sm="20">
                <Grid item>
                  <Input
                    value={this.state.studentsNum}
                    margin="dense"
                    onChange={this.handleStudentsNumChange}
                    onBlur={this.handleBlur(this.state.studentsNum)}
                    inputProps={{
                      step: 1,
                      min: 0,
                      max: 120,
                      type: 'number',
                      'aria-labelledby': 'input-slider',
                    }}
                  />
                </Grid>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                תיאור :
                </Form.Label>
              <Col>
                <Input fullWidth value={this.state.about} onChange={this.handleAboutChange} />
              </Col>
            </Form.Row>

            <br />
            <Button variant="contained" type="submit" className={classes.btnSubmit} onClick={this.updateCourse}>
              עדכון קורס
                </Button>
          </Form>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(EditCourse)
