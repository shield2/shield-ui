import React from "react";
import { Form, Col } from "react-bootstrap";
import { styles } from "../Courses/Style.css.js";
import { Button, Paper, Input, Grid, withStyles } from "@material-ui/core";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";

class AddDefense extends React.Component {
  constructor(props) {
    super();
    this.state = {
      courseId: props.courseId,
      lecturer: "",
      id: "",
      date: new Date(),
      time: 15,
      startTime: "",
      endTime: ""
    };
  }

  handleSubmit = () => {
    
    let newDefense = {
      courseId: this.state.courseId,
      date: this.state.date.toISOString(),
      defenseLength: this.state.time,
      startTime: this.state.startTime.toISOString(),
      endTime: this.state.endTime.toISOString(),
    }
    console.log(newDefense)

    axios.post("http://localhost:8080/addDefense", newDefense, { withCredentials: true }).then(res => {
          console.log(res)
          console.log("success")
        }).catch(err => {
          console.log("error")
        })
    alert(`הוספת הגנה חדשה`);
  };

  handleLecturerChange = evt => {
    this.setState({ lecturer: evt.target.value });
  };

  handleIDChange = evt => {
    this.setState({ id: evt.target.value });
  };

  handleDateChange = date => {
    this.setState({ date: date });
  };

  handleTimeChange = evt => {
    this.setState({
      time: evt.target.value
    });
  };

  handleStartTimeChange = time => {
    this.setState({ startTime: time });
  };

  handleEndTimeChange = time => {
    this.setState({ endTime: time });
  };

  handleBlur = value => {
    if (value < 0) {
      this.setState({ time: 0 });
    } else if (value > 60) {
      this.setState({ time: 60 });
    }
  };

  render() {
    const { classes } = this.props
    //ToDO get course name by id from api
    //const CourseName = "אפליקציות אינטרנטיות";

    return (
      <div className={classes.root}>
        <div className={classes.mainTitle}>
          <h1> הגנה חדשה לקורס</h1>
        </div>
        <Paper elevation={3} className={classes.paper}>
          <Form className={classes.Papertext}>
            <Form.Row>
              <Form.Label column sm="4">
                תאריך ההגנה:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.date}
                  onChange={this.handleDateChange}
                  dateFormat="dd/MM/yyyy"
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                זמן הגנה (דקות) :
              </Form.Label>
              <Col sm="20">
                <Grid item>
                  <Input
                    defaultValue={this.state.time}
                    value={this.state.time}
                    margin="dense"
                    onChange={this.handleTimeChange}
                    onBlur={this.handleBlur(this.state.time)}
                    inputProps={{
                      step: 1,
                      min: 0,
                      max: 60,
                      type: "number",
                      "aria-labelledby": "input-slider"
                    }}
                  />
                </Grid>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                שעת התחלה:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.startTime}
                  onChange={this.handleStartTimeChange}
                  showTimeSelect
                  showTimeSelectOnly
                  timeIntervals={15}
                  timeCaption="Time"
                  dateFormat="HH:mm"
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                שעת סיום:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.endTime}
                  onChange={this.handleEndTimeChange}
                  showTimeSelect
                  showTimeSelectOnly
                  timeIntervals={15}
                  timeCaption="Time"
                  dateFormat="HH:mm"
                />
              </Col>
            </Form.Row>

            <br />
            <Button
              variant="contained"
              type="submit"
              className={classes.btnSubmit}
              onClick={this.handleSubmit}
            >
              יצירת הגנה
            </Button>
          </Form>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(AddDefense)