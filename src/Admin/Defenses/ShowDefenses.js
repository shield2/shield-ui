import React from "react";
import { styles } from "../Courses/Style.css.js";
import { withStyles, Button, Card, CardContent, Typography, CardActions, Grid, Fab, Tooltip }
  from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import { navigate } from "@reach/router";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import axios from 'axios';

class ShowDefenses extends React.Component {
  constructor(props) {
    super(props);
    //const defenses = this.getAllDefenses()
    this.state = {
      defenses: [],
    };
  }
  componentDidMount() {

    console.log(this.props.courseId);

    axios.get("http://localhost:8080/getDefenses?id=" + this.props.courseId).then(res => {
        const defensesD = res.data;
        this.setState({defenses: defensesD});
        console.log(defensesD)      
      }).catch(err => {
        console.log("error")
      })

  }

  // getAllDefenses = () => {
  //   const defenses = [
  //     {
  //       date: "21/2/2020",
  //       lecturer: "איגור רוכלין"
  //     },
  //     {
  //       date: "27/2/2020",
  //       lecturer: "איגור רוכלין"
  //     },
  //     {
  //       date: "19/2/2020",
  //       lecturer: "אליאב מנשה"
  //     },
  //     {
  //       date: "24/2/2020",
  //       lecturer: "אליאב מנשה"
  //     },
  //     {
  //       date: "1/3/2020",
  //       lecturer: "אליאב מנשה"
  //     }
  //   ]
  //   return defenses;
  // }

  render() {
    const { classes } = this.props
    //TODO get course name from api
    //const courseName = "אפליקציות אינטרנטיות"

    const handleClickOpen = () => {
      this.setState({ open: true });
    };

    const handleClose = () => {
      this.setState({ open: false });
    };

    return (
      <div className={classes.root}>
        <h1 className={classes.mainTitle}> הגנות לקורס </h1>

        {/* <Tooltip title="הגנה חדשה" aria-label="add">
          <Fab
            className={classes.btnAdd}
            aria-label="add"
            type="button"
            size="small"
            onClick={() => {
              navigate("/Defenses/AddDefense");
            }}
          >
            <AddIcon />
          </Fab>
        </Tooltip> */}

        <Grid container spacing={2}>
          {this.state.defenses.map(elem => 
            <Grid item key={this.state.defenses.indexOf(elem)}>
              <Card className={classes.defenseCard}>
                <CardContent>
                  <Typography className={classes.text} gutterBottom variant="body1" component="h2">
                    תאריך: {elem.date}
                  </Typography>
                  <Typography className={classes.text} variant="body2" color="textSecondary" component="p">
                    מרצה: {elem.lecturer}
                  </Typography>

                </CardContent>
                <CardActions className={classes.cardButton}>
                  {/* <Fab
                    className={classes.deleteBtn}
                    size="small"
                    aria-label="delete"
                    onClick={handleClickOpen}
                  >
                    <DeleteIcon />
                  </Fab> */}
                  {/* <Fab
                    className={classes.editBtn}
                    size="small"
                    aria-label="edit"
                    onClick={() => navigate('/Defenses/EditDefense/')}
                  >
                    <EditIcon />
                  </Fab> */}
                  {/* <Dialog
                    className={classes.dialog}
                    open={this.state.open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                  >
                    <DialogTitle id="alert-dialog-title">
                      {"אישור מחיקה"}
                    </DialogTitle>
                    <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                        האם אתה בטוח שברצונך למחוק את ההגנה?
                      </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={handleClose} className={classes.editBtn}>
                        כן
                      </Button>
                      <Button onClick={handleClose} className={classes.deleteBtn}>
                        לא
                      </Button>
                    </DialogActions>
                  </Dialog> */}
                </CardActions>
              </Card>
            </Grid>
          )}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(ShowDefenses)
