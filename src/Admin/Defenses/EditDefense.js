import React from "react";
import { Form, Col } from "react-bootstrap";
import { styles } from "../Courses/Style.css.js";
import { Button, Paper, Input, Grid, withStyles } from "@material-ui/core";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class EditDefense extends React.Component {
  constructor(props) {
    super(props);
    const defense = this.getDefenseDetails();
    this.state = {
      lecturer: defense.lecturer,
      id: defense.id,
      date: defense.date,
      time: defense.time,
      startTime: defense.startTime,
      endTime: defense.endTime
    };
  }

  getDefenseDetails = () => {
    // TODO: Use defenseId to get defense from API
    const today = new Date();
    const course = {
      lecturer: "איגור רוכלין",
      id: "123456789",
      date: today,
      time: "15",
      startTime: today,
      endTime: today
    };

    return course;
  }

  handleSubmit = () => {
    alert(`עדכון הגנה`);
  };

  handleLecturerChange = evt => {
    this.setState({ lecturer: evt.target.value });
  };

  handleIDChange = evt => {
    this.setState({ id: evt.target.value });
  };

  handleDateChange = date => {
    this.setState({ date: date });
  };

  handleTimeChange = evt => {
    this.setState({
      time: evt.target.value === "" ? "" : Number(evt.target.value)
    });
  };

  handleStartTimeChange = time => {
    this.setState({ startTime: time });
  };

  handleEndTimeChange = time => {
    this.setState({ endTime: time });
  };

  handleBlur = value => {
    if (value < 0) {
      this.setState({ time: 0 });
    } else if (value > 60) {
      this.setState({ time: 60 });
    }
  };

  render() {
    const { classes } = this.props
    //ToDO get course name by id from api
    const CourseName = "אפליקציות אינטרנטיות";

    return (
      <div className={classes.root}>
        <div className={classes.mainTitle}>
          <h1> עדכון הגנה לקורס {CourseName}</h1>
        </div>
        <Paper elevation={3} className={classes.paper}>
          <Form className={classes.Papertext}>
            <Form.Row>
              <Form.Label column sm="4">
                שם המרצה :
              </Form.Label>
              <Col sm="20">
                <Input
                  defaultValue={this.state.lecturer}
                  placeholder="שם המרצה"
                  onChange={this.handleLecturerChange}
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                תעודת זהות מרצה :
              </Form.Label>
              <Col sm="20">
                <Input
                  defaultValue={this.state.id}
                  placeholder="תעודת זהות מרצה"
                  onChange={this.handleIDChange}
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                תאריך ההגנה:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.date}
                  onChange={this.handleDateChange}
                  dateFormat="dd/MM/yyyy"
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                זמן הגנה (דקות) :
              </Form.Label>
              <Col sm="20">
                <Grid item>
                  <Input
                    value={this.state.time}
                    margin="dense"
                    onChange={this.handleTimeChange}
                    onBlur={this.handleBlur(this.state.time)}
                    inputProps={{
                      step: 1,
                      min: 0,
                      max: 60,
                      type: "number",
                      "aria-labelledby": "input-slider"
                    }}
                  />
                </Grid>
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                שעת התחלה:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.startTime}
                  onChange={this.handleStartTimeChange}
                  showTimeSelect
                  showTimeSelectOnly
                  timeIntervals={15}
                  timeCaption="Time"
                  dateFormat="h:mm aa"
                />
              </Col>
            </Form.Row>

            <Form.Row>
              <Form.Label column sm="4">
                שעת סיום:
              </Form.Label>
              <Col sm="20">
                <DatePicker
                  selected={this.state.endTime}
                  onChange={this.handleEndTimeChange}
                  showTimeSelect
                  showTimeSelectOnly
                  timeIntervals={15}
                  timeCaption="Time"
                  dateFormat="h:mm aa"
                />
              </Col>
            </Form.Row>

            <br />
            <Button
              variant="contained"
              type="submit"
              className={classes.btnSubmit}
              onClick={this.handleSubmit}
            >
              עדכון הגנה
            </Button>
          </Form>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(EditDefense)
