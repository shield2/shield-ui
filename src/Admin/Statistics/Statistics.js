import React from "react";
import { Form, Col } from "react-bootstrap";
import { Paper, withStyles, Button } from "@material-ui/core";
import { styles } from "../Courses/Style.css";
import BarChart from "./BarChart.js";
import axios from 'axios';

  const freeDefense = [
  { date: "20/01/2012", count: 5 },
  { date: "21/01/2012", count: 5 },
  { date: "22/01/2012", count: 6 },
  { date: "23/01/2012", count: 12 }
];

const freeDefense2 = [
    { date: "25/01/2012", count: 10 },
    { date: "26/01/2012", count: 20 },
    { date: "27/01/2012", count: 30 },
    { date: "28/01/2012", count: 40 }
  ];



class Statistics extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          barData: "",
          avgInCourse:[],
          defensesByCourse:[]
        };
      }

      componentDidMount() {

        console.log(this.props.courseId);
    
        axios.get("http://localhost:8080/getCoursesWithAvgStudents").then(res => {
            const courseWithNumbers = res.data;
            //this.setState({defenses: defensesD});
            //console.log(defensesD)   
            courseWithNumbers.forEach(elem => {
              var c = {date: elem.name, count: (elem.minStudents+elem.maxStudents)/2}
              this.setState({avgInCourse: [...this.state.avgInCourse, c]})
            });
          }).catch(err => {
            console.log("error")
          })

          axios.get("http://localhost:8080/countDefenseByCoures").then(res => {
            const DefenseWithNumbers = res.data;
            //this.setState({defenses: defensesD});
            //console.log(defensesD)   
            DefenseWithNumbers.forEach(elem => {
              var c = {date: elem._id.date, count: elem.count}
              this.setState({defensesByCourse: [...this.state.defensesByCourse, c]})
            });
          }).catch(err => {
            console.log("error")
          })
      }

  render() {

    
    const { classes } = this.props;

    const showFreeDefense = () => {
        this.setState({ barData: this.state.avgInCourse },() => {
          console.log(this.state)
        })
    };

      const showFreeDefense2 = () => {
        this.setState({ barData: this.state.defensesByCourse });
      };

    return (
      <div className={classes.root}>
        <h1 className={classes.mainTitle}> דו"חות למנהל</h1>
        <Paper elevation={3} className={classes.paper}>
          <Form className={classes.Papertext}>
            <Form.Row>
              <Form.Label column sm="4">
                בחר דיאגרמה להצגה:
              </Form.Label>
              <Col sm="20">
                <Button className={classes.diagramBtn} onClick={showFreeDefense}>
                  ממוצע סטודנטים לקורס
                </Button>
                <Button className={classes.diagramBtn} onClick={showFreeDefense2}>
                    כמות הגנות לקורס
                </Button>
              </Col>
            </Form.Row>
          </Form>
        </Paper>
        <Paper elevation={3} className={classes.paper} >
            <BarChart data={this.state.barData} />
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(Statistics);
