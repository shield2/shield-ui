import React from 'react';
import { styles } from '../Admin/Courses/Style.css';
import { withStyles } from "@material-ui/core";
import './About.css';
import axios from 'axios';
import { subscribeToTimer } from './socketApi'

class About extends React.Component {

    constructor() {
        super();
        this.state = {
            weather: "",
            timestamp: "Loading...",
        }
        subscribeToTimer((err, timestamp) => {
            this.setState({ timestamp })
        })

    }

    componentDidMount() {

        this.updateWeather()
        setInterval(this.updateWeather.bind(), 600000);

        this.updateCanvas();
    }



    updateWeather() {
        axios.get("http://localhost:8080/getWeather")
            .then(response => {
                this.setState({
                    weather: `טמפ' במשך היום: ${response.data.main.temp_max}°c ו${response.data.weather[0].description}`
                });
            })
    }

    updateCanvas() {
        const ctx = this.refs.canvas.getContext('2d');

        var imageObj1 = new Image();
        imageObj1.src = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBUQEBAQEBAQERAQDxAQDw8PEBAQFRYYFhcVFRUYHSggGBolGxUWIjEiJSk3Li8uFx8zODMtNygtLisBCgoKDg0OGhAQGi0lICUyLzMtLS0tLSstLS8tKy0rLTUtLS0tLS0tLS0tLS8rLS4uLS0tLS0rLS0tKy4tLS0tLf/AABEIAQQAwgMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABQIDBAYHAQj/xABJEAABAwICBgYFCQQIBwEAAAABAAIDBBEFIQYSEzFBUQciYXGBoRQyQlKRIyRicoKiscHRM5LC8ENTc5OjsrPhJTREY3TD4hX/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIDBAUG/8QALhEBAAIBAwIEBAYDAQAAAAAAAAERAgMSITFBBCJRYXGx0fATMoGRocEjQ9IF/9oADAMBAAIRAxEAPwDuKIiAiIgIiICIiAseurYoGGSZ7Y2N3ucbDsA5k8AMyva6sjgjdLK8MjYNZznbgPz7l8+6eacyV0vyZLImXEQv6o3E/XI48Nw5mJlMRaa6R+k2SUOpaIuiYbtkkBtKW8RceqTyGY4m+Q0vRLTGvwsjYPLoH3Po02sYHgGziz3De4u3jvBtZYGF4W+d7Whrnazg1rWi7pHE5NC71gOg8AoPQ61kc5c4zGI2+blzQ20Th1mmzfWB33sqRly2nSrDdP6Q90T6T6Cu1WPd6JUusNjO4Bjnco5fVd2A2ceS3dcL0q6IpY9Z9C/bMzPo0xa2UDPJkhs1/AWNu8rW8L0oxbCX7ESSxhv/AEtUxz47DLqtdmG5ewQFbcy2y+mEXKMD6aInWbW0z4eBlgdtmd5YbOb3DWW8YVpphtVYQ1sBc7dG9+xl/u5LO8lNoqU+i8BvmMwvVKBERAREQEREBERAREQEREBFbqKhkbS6R7WNG9z3BrR3krUMa6TMNp7hkpqnjc2mGu3+8NmfAnuUTNJiJno3NQOk2l1Hh7bzyjaWuyCOz539zeA7XWHauTaQ9KVdU3ZBq0cZ/qztJiO2Qjq/ZAPatHcHOcXOJc5xu573FznE8STmSqzm1x0ZnqntNtNKjE5LO+Sp2H5Kna4kfWkPtP8AIbhxJjMDwGaqlEUUZkec9UZAN4ue7c1o5n/ZbRoz0fzzja1B9EpwNYvkAErm82sPqj6TvgVPVuk9NRRGmwpjW3/aVJGsXHdcF2bz2nIcAQs8svV1aOhOU1jFz/EfFlwRUmBRXOpU4k9mQHqxA7re4zt9Z3YN1no7xyWXEZHTvL31MRuT70ZDmtaOADS+wWjSyue4ve4uc43c5xLnOPMk71n6N1exrIJODZWg9jX9R3k4rH8TmPR6s+DiNLKJ5ymOv09neHPB35rAxLC4KhmzljjkYfYlY2RnwO4qvaJtFvOVvCjCY6NExjoppH3MO1pznlG7ax/uP63gCFp2I9F9Yz9nJBMOTtaF5+y4EfeXbNqvTNffY96i01PeHz43R7FqQfJw1kI3/NZHnx+QcVebpLjUORqa9lv61r3H/FaV3ktjO9g8MvwVBgj+kPFTclY+kuEnpAxgZenyjvipb/6a9j09xVsjJH1k8gY9khj+TYJGtcCWENaMiAR4runosfvP+I/Rc16X6CwilFyWPMRPNj26wv3FhH2k3SnZjPT5OyU07ZGNkYbse1r2EcWuFwfgVcWodFGIbfC4RfrQa1O7O9tQ9QfuFi29bQ5pipoRERAiIgIiIIrSHH6ehi2s7rDcxgsXyO5NHHv3DiuK6RdItbVSnYzPpYhkI4Xarrc3SDrE91h2LW9MMYnnrJxLI5745pobu9kRvc3VaNzRluCisPHWPaFlMzLqwjCJjGOff6M+rnfKdaWR8rhudK90jvi4kqmngdI4MjY6R53MY0vcfAZqb0Np4ZMQhinjbJHIJG6r7lusGOcCRxzbbxW4Ynpa6ke+mpaWCARPcy4aLGx9YMaGgX353VJmIi5dGGnnnltwhCYP0cVcvWqHMpI951iJJLfVabDxd4KbhqsKwz/lo/TKpv8ATPIcGu5h9tVv2B4rV8Rxioqf28z5B7pOqwfYbZvksG6znU9Hdp+Aj/ZN+0dEtjekNTWH5Z/UvcRMu2IdtvaPabqKXl0uspmZ6vRwjHCKxioeoV5dLotbtWH1m1hjl/rI43+Lmg/msjaLV9DKzXo2C+cZfGfA3H3XBTm0WkS8HPTrKYZm0TaLD2ibRTauxmbRNosPaJtEs2MzaLXekKl21G/K52ZcPrRnXHxsQpbaKxinWpzfPUcCe45H8VN8SjbWUffVrfQbX2fUUxOTmsqGDtHUefOP4Lra4H0eSmmxeJnBz5aZ/aCDb7zWLvi6MJuHJ4jDbmIiK7AREQEREHzFp/SiPFaxtrfOHPt/aASfx38VEUos4fD4reemSh1MULwMp4IZCebhrRHyjb8QtLY3jyWWUurSx6SkaGp2FTTzXsI6iIuP0NYa33brZdOodSukPvtjf90NPm0rUcUjvE7ssf58Ctw0om28FHV2zmp2h/Y4AOt8XP8AgsZ5wejp/wCPxPxj7+TXrpdU3S6yejuVXS6pul0Nyq6XVN0uhubhoFVW2sf1JB5td/Ctt2i51opPqVTfptew/DWHm0Le9ojj1sfNbK2ibRYu1TapbLaytom0WLtU2qWbWVtFcZ12PZ7zCB3/AMlYO1V6jm647bhTjPKmpj5Zc1rJzBXNnH9HLDOLfRLXHzaV9FNIIuNxzHcvnzSqC05HIvZ4Bxt5Fdu0SqtrQ00hN3Ogi1j9MNDXeYK30J7MfH4cY5wlkRF0PNEREBERByrpyobilqBwdLA7tLgHt/yP+K5W1i750pUO2wyU260Lo5m9mq4B33HOXCgxYanEvR8JjuxXhHrs1ebS38lNYO10uCBrg4PpJ35OBB1S4nK+8Wl+6oePILZtEna4mpzunhcBfcHAFv4Ov4LHHLrHq9DX0onHHV74tYsll6QRkciMiORXiq34LJZEQ4F4vUQXaOfZyMf7j2uPcDn5Lom0XNSFuWF4kx7GjWGuGtDgciSBY25+CjJnljMpjaJtFja6a6pam1k7RNosbXTXSzaydoq4ZbOB7R+Kw9dNopiScLikHpkz5w7vaR3Fo/MLpPRbPrYaxvGKSZh/fLx5PC51pt+1a73omHxDnD8LLcehua9POz3Zw4faY0fwLo0+NSWHiYjLweM+lfR0JERdTxRERAREQY+I0jZoZIXerLG+N3c4Efmvm90DmOLHCzmOLHjk5psR8QV9MLhnSHQbDEJQBZs1p2/b9b74esNeOIl6n/lzE5zhLWnDJSGj1Vs5mOvueL/Vd1XeRUeSqaV9ngczqnxXND2NSI6dpSWk1NsqqQcHnaN7n5n72t8FF3Wx6UDaQQz8ReKQ9vC/i13xWsrSY5cellO2p6xx+yu6XVCKGlq7pdUIhau6uxHJY6uwHeqzDTSy8yVo8VkZk467eROY7ipqmrWyC7T3g7x3haqqmPLTcEgjcQqU6MtKJbdrprqJosRDuq7J3Pg79Cs3XVXPOFdWTrprrG1010RSxpkerA7nG4fDV/VbJ0LTdaqZ2U7x/iA/ktZ0sN6eA9rx8R/spjoXl+dTt96nDv3XgfxLqw/PE/fRw6k34XLH0/6dfREXU8YREQEREBc26ZMPJZDVD2HOgf3O6zT4Frh9oLpKidKsK9Lo5oMtZ8ZMd+Eresz7wCrnjeNN/Dav4erjk+eiVYlNjdVk+HMHIhW5Ny44h9HqTcNvw35zSvi4vbrs7JB/9Aea1NTGilZquLfdOsPqnJ35fFY+kVLs6h1vVk+Ub9rf538lbs4Zms/jz+vdHoqLpdF9ytFRdLoWrVURzCtXXoKUnHKptmovNZLrJ6e56pCjrvZce536qOul1FKzUp/XTXUXS1Xsu8D+SytoopntXdKX/NIT/wB0jyf+ikuhl/8AxF450kvlJF+qhtKH/M4P7X8nqU6GT/xN3/hzf6kK68Y6PHynyake8/N29ERdDyhERAREQEREHz90gYV6JiErALRynbxfVkuSB3PDx3ALWyV2LplwjaUrKpo69M/VeQP6GSwPwdqdwJXGiVy541L3vDa2/Sif0XKCp2UrX8AbO+qd/l+C2vSGn2lOHjN0J+LDYH8j4FaXIty0YqxLBqOzLBs3jmwjqn4ZeBUd7Z6kcfDmGrXS6u4hTGGV0Z9k9U82nMH4LHuppEZWrul1RdLpRuV3S6oul0o3M2I3AVSsUzsiFdWUxy9DTzvGFSKlFC+5UsqGa+R3/isNeh1kmExkztKX/Nacc5CfI/qp3oUbfEZDyo5B8ZYv0WsaXSfJ0jObXP8AJn6lbf0Fx3qql3uwRt/eeT/AurGOjwc8uNSff+3ZkRFs4BERAREQEREFiupGTxPhkGtHKx0b282uFj+K+ZsZw59JUSU0nrwvLCd2s3e1/c5pB8V9QLk3TZgFtniDBygqP/W/4ktPexZ6mNxbs8Hq7cts93KiVm6P1+wnaXGzH9STlY7j4Gx7rqOJVt6yq+HfnPeG76WUN2iUb2ZO7WHj4H8StVutv0ZrfSaYsf1pIRqPBz14yOqT3i4729q1fEaQwyFh3b2nm07j+Xgkcuf8s1HTssXS6oul0Tau6XVF0uhbIpnZ+CyVgxusR3rMuqZRy69DPy0qRU3S6pTo3KkKpurtI3WkY3m9o8L5qaROdRbaNKMDhfhJqnZTUk8bYnD22P2cbmHsu7W72dpUv0CwZVcnM00fdqiR38Y+Cj9O6nZ4PDGN89SC76rdd34tYtj6DKXVw+SQ/wBNVPI+qxjGf5g5dWPV4eXGlPu6MiItHKIiICIiAiIgLDxjDo6qCSnlF2TMdG7mLjIjtBsR2gLMRCJp8q4vQSUs8lNKLSQvLHcjxDh2OaQ4dhCwSV2Xpt0Z2kTcQibd8IEdTYZuhJ6rz9Vxz7HE+yuLlyxmKl6mnq78bSOBYoaWdsuZb6sjR7UZ3+I3jtC3LSjDRJGJI7Oy14yMw5pFy0d4sQuday3fQTFRK00MpzsXUzic8syzwzI7NYcAqzxyrMXx+zV7pdS+kuGGGQutZrjZw91/6Heoa6lSMrVXS6pul0Taq6zmuuLqPusunddvdkqZQ30Mqml66XVKKlOrcqus/BGXmB90Od+X5qOWw6IU2tJc7i5o8G9Z35K2McstbLyT7vOlKpsaWmB/YwF7h2vIaD/hu+K6z0ZUWxwmlbuL4tued5iZf41w3Sxzq3FHxMNy+aKkjIzserH/AJy4r6Wp4WsY1jRZrGtY0cmtFh+C3044eZ4ia4XERFo5hERAREQEREBERBRNE17SxwDmuBa5pFw5pFiCOIsvmXT3Rl2G1jobHYPvJSvNzrRE+qTxc3cfA8V9OrWOkHRVuJ0bohYTxnaUzzkGygeqT7rh1T3g7wFExbXS1NsvmZVRSuY4PY4tc0hzXDe1wNwQvaiB8b3RyNLJI3OY9jhZzXtNiD4q3dZuuZdRo6lmKUhksNvG3UqY25X5OaO21x4jeFotfSOheWnMb2u4ObzVnR/GZKKds8eYHVkZfKSMkXb2HK4PAgdy3/H8NhrIG1VOdaKTrXAzifuNxwzyI4HwVenCmXPm/dz66XXs8bmOLXCzgbEfzwVu6lW1d1kUjt48ViXV2mf1u+4UT0X08qyhnXS6pul1k77VXW4YK8U0D5nDKGJzj2u9a3flbxWq0MWvIBw3u7h/PmpPTOr2VIyAHrTv1njjqNs4+ep5qa7erHUyufhz9FHRLh7qrF43uzEAlqpTbe62qO468jT4FfRi5b0C4Ps6WascM6mTZxn/ALUNwT4yF4+wF1JdWPR5mpleQiIpZiIiAiIgIiICIiAiIg5V0x6DmdpxGlZeaNvzqNozliaMpABve0Cx5tH0QDxG6+w1wbpZ0A9Dc6upG/NXuvNE0f8ALPcfWHKIn90nkRasw209TtLmi2LQvSh1BKQ8GSlmsJ499uGu0e9beOIy4C2urxUq2m6nT9K9HGSRtqadwfBIA6GVuYaHZhrvongeG7fv0CWNzHFrgQ4ZEFTOhOl7qFxhmBlopSdrFbWMZO97AfNvHv37TpTovHLEKqlcJaZ7daOVh1zGOTubb88wbg5746dVZ9Yc6uvWOsQeRC9qIHRu1Xix8iOYPEK0ppFpW6XVuJ12g9iyKWHXcG8N5PILOnob+LTOj9N7R9rP7I/U/ktdxuofXVgZCNcueynp28HEu1Qe4uJN+VlPY1X+j05DcpJeoy3si2Z8B5kKV6C9HttVvrXt+SpBqRE7jUPGdvqxk/3jeSthFzbn1stuNT1nmf6dowDC2UdLDSsN2wRMjDjvcQM3HtJufFSCIt3AIiICIiAiIgIiICIvCg9uvCVQ5yxZp7KE0yzKFZnkY5pa8Nc1wLXNcAWuaciCDvBCiaitsouoxPtUWmnJuknQc0ErqimaXUMjrgC5NK4+w/6F/Vd4HOxdot136sxIOa5jwHMcC1zXAOa5pFiCDvBC5LpVo6IXGWnu6E5ujzLovHe5vmOPNVXiWtXWx6GaYT4bJkNrTSH5enJADri2uwn1X28CBY8CNbS6kt2LFNHqXEYPSsNImiPrwNylhfa5DWnNp+gfC4suaYjh8kJNwS0G2ta1jycPZKsYFjlTQzCellMb8g4esyRt76sjdzh5jgQc11PDMcw3HAI5tWhxJw1c7GKoO4WJyffLqmzuA1gFFItzOiddtuR/FbHh8Ajbd1gT1nE8ByWZi2iElBLeSMtaT1XNOtC53Np4HsPwWs6Q4lkYWHf+0PIe7+qzy61Ds0pjZuy6R/M9o+rDxCd9ZUtbE0uL3NhgZnnc2Hdcm/YO5fSeiWHRYfRxUkZB2bflH2ttJXZvf4uJy4Cw4LiPR1Qtjd6ZIBr5tpwR6oOTpO8i4HZfmukwYuTxWsVHEOTPKc5mZb+2oBVYlC1CnxMnipSnrb8Va1KToevbrCimushrkRS7deqi68UoXEREBERAVLlUqXoMeZyiqyVSU6hq9qrK0IeuqVAVlYpOvaVrtcwqqzDq6481D1NeeavVjCoepaVKEZiVM0kuZZpOZbwPdyUWVKzsKwZYyiGOvDnvVTmkKhSW2iLTuu9EfRSyekRO1Nk6Yl00DmuBBa/e4ZEWdffkQoKhh2j7uuW3u48XHl4qzHA49ikqaIjIKE7pquzZKWutYDICwA3ABTFHXHmtWpmFTFGwoNtoqxT9DUrUqFhWxYe0oNqo5bqTidkoahapeIKUL90XlkUoZCIilAiIgLwr1EGNIxR1TCpctVmSJQtEtSraRQNZRdi32ekvwUbUYbfgq0m3Oqug7FEVOHHkulVGE9iwJsG7FCXMpsNPJYcmGnkumS4J2LFfgXYhTm5w08lSzCuOqujf/g9i8bgPYlopoUeGnksyDDTyW7x4F2LJiwTsRNNRpsOPJS9JQdi2WHBuxZ0GE9iCHo6LsU9RUizKfDbcFJwUllKFFJDZSEbV7HFZXQ1WVmVNkVyyIPURFKBERAREQF4QvUQUFqtOjHJX0soSwnwDkrL6UclJaqpLAlJtFOoxyVBoByUvswvNmootD+gDkgw9vJTGzTZpRaJFAOSrbRDkpTZpqJRbCbSjkrrIByWVqL0BTSLWmxq4Gqqy9QeAL1EUoEREBERAREQEREBERAREQEREBeIiBZLIiD2yIiAiIgIiICIiAiIg/9k='
        imageObj1.onload = function () {
            ctx.drawImage(imageObj1, 0, 0, 300, 300);
        }
    }

    render() {
        const { response } = this.state;
        const { classes } = this.props
        return (
            <div className={classes.root} >
                <div>
                    <h1 className={classes.mainTitle}> קצת עלינו</h1>
                </div>

                <div className="tran"></div>
                <br />
                <header>
                    <h4>למה shield משמש ?</h4>
                </header> <br />
                <section className="introduction">
                    <br /> נמאס לכם להירשם להגנה בExel שיתופי ? <br /> <br />
                    חוששים שידרסו לכם את הרישום ? <br /> <br />
                    מפחדים מהתנגשויות ? <br /> <br />

                    <div className="ShieldText">
                        <div>
                            <canvas ref="canvas" width={300} height={300}> </canvas><br />
                            Shield
                        </div>
                    </div>
                    <div className="boldedText">
                        פה להגן עליכם! <br />
                    </div>

                    <br />
                    מערכת רישום אוטומטי להגנות- <br />
                    המסלול האקדמי המכללה למנהל.<br /><br />
                    <aside>תקופת מבחנים מוצלחת! </aside>
                    <br />
                    <br />
                </section>
                <br />
                <br />
                <div>
                    שירותי מזג האוויר במכללה למנהל מציגים
                <br />
                    <div>{this.state.weather}</div>
                    <br />
                    <br />
                </div>
                <div>שירותי socket מבית היוצר - מציגים שעה בחסות צד שרת
                        <br />
                    <div>{this.state.timestamp}</div>
                </div>

                <section id="video" text-align="center">
                    <video width="370" height="270" float="center" display="block" controls autoPlay>
                        <source src="/Videos/ShieldTrailer.mp4" type="video/mp4"></source>
                        Your browser does not support the video tag.
                    </video>
                </section>

                <footer>
                    פותח ע"י: רון אזואלוס, מאי גליבטר, אדיר חנוני<br />
                    במסגרת קורס נושאים מתקדמים באפליקציות אינטרנטיות <br />
                    שנת הלימודים תש"פ <br />
                </footer>
            </div >


        );
    }
}

export default withStyles(styles)(About);