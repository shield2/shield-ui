import React from "react";
import { Router, Link, navigate, Location } from "@reach/router";
import {
  AppBar,
  Toolbar,
  Typography,
  ListItemText,
  ListItem,
  Drawer,
  List,
  withStyles
} from "@material-ui/core";
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { styles } from "./App.css.js";
import axios from "axios";
import Home from "../HomePage/Home";
import ErrorPage from "../errorPage/ErrorPage";
import MyDefense from "../myDefense/MyDefense";
import EditRegister from "../DefenseRegister/EditRegister.js";
import Register from "../DefenseRegister/Register.js";
import Login from "../loginPage/Login";
import AdminHome from "../Admin/Courses/AdminHome.js";
import EditCourse from "../Admin/Courses/EditCourse.js";
import AddCourse from "../Admin/Courses/AddCourse.js";
import ViewUsers from "../Admin/Users/ViewUsers";
import Statistics from "../Admin/Statistics/Statistics.js";
import About from "../About/About";
import AddDefense from "../Admin/Defenses/AddDefense.js";
import ShowDefenses from "../Admin/Defenses/ShowDefenses.js";
import EditDefense from "../Admin/Defenses/EditDefense";
import Facebook from "../FacebookPage/Facebook";
import Component from "@reactions/component";
import "./globalDesign.css";
axios.defaults.withCredentials = true;

const theme = createMuiTheme({
  typography: {
    fontFamily: [
      'VarelaRound',
      'sans-serif'
    ].join(','),
  }
});

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      userId: "",
      userName: "",
      isAdmin: ""
    };
  }

  render() {
    const { classes } = this.props;
    const track = async pathname => {
      if (document.location.pathname !== "/login") {
        if (document.cookie === "") {
          // document.location.pathname !== "/error") {
          await navigate("/login");
        } else {
          axios
            .get("http://localhost:8080/getUser")
            .then(response => {
              this.setState({
                userId: response.data.id,
                userName: response.data.userName,
                isAdmin: response.data.isAdmin
              });
            })
            .catch(error => {
              console.log(error);
              navigate("/login");
            });
        }
      }
    };

    function deleteAllCookies() {
      var cookies = document.cookie.split(";");
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }
      this.setState({ userName: "" })
    }
    return (
      <ThemeProvider theme={theme}>
        <div>
          <div className={classes.root}>
            <AppBar
              color="default"
              className={classes.appBar}
              position="static"
              id="allSite"
            >
              <Toolbar>
                <Typography className={classes.userInfo}>
                  שלום {this.state.userName} ברוך הבא
                </Typography>
                <Typography variant="h6" className={classes.siteInfo}>
                  Shield - מערכת הרשמה להגנות
                </Typography>
              </Toolbar>
            </AppBar>
            {
              (this.state.userName !== "") ? (
                <Drawer
                  anchor="right"
                  className={classes.drawer}
                  variant="permanent"
                  classes={{ paper: classes.drawerPaper }}
                >
                  <div className={classes.toolbar} />
                  <List>
                    <Link to="/">
                      <ListItem className={classes.linkText} button>
                        <ListItemText primary="דף הבית" />
                      </ListItem>
                    </Link>
                    {
                      (this.state.isAdmin) ? (
                        <div>
                          <Link to="/AdminHome">
                            <ListItem className={classes.linkText} button>
                              <ListItemText primary="דף מנהל" />
                            </ListItem>
                          </Link>
                          <Link to="/ViewUsers">
                            <ListItem className={classes.linkText} button>
                              <ListItemText primary="צפייה במשתמשים" />
                            </ListItem>
                          </Link>
                          <Link to="/Statistics">
                            <ListItem className={classes.linkText} button>
                              <ListItemText primary="דו''חות מנהל" />
                            </ListItem>
                          </Link>
                        </div>
                      ) : null}
                    {/* <Link to="/MyDefense">
                      <ListItem className={classes.linkText} button>
                        <ListItemText primary="ההגנות שלי" />
                      </ListItem>
                    </Link> */}
                    <Link to="/Facebook">
                      <ListItem className={classes.linkText} button>
                        <ListItemText primary="פייסבוק" />
                      </ListItem>
                    </Link>
                    <Link to="/About">
                      <ListItem className={classes.linkText} button>
                        <ListItemText primary="אודות" />
                      </ListItem>
                    </Link>
                    <Link to="/login" onClick={() => deleteAllCookies()}>
                      <ListItem className={classes.linkText} button>
                        <ListItemText primary="התנתק/י" />
                      </ListItem>
                    </Link>
                  </List>
                </Drawer>
              ) : null}
          </div>

          <Router>
            <Login path="/login" />
            <Home path="/" />
            <ErrorPage path="/error" />
            {/* <MyDefense path="/MyDefense" /> */}
            <EditRegister path="/DefenseRegister/EditRegister/:registerId" />
            <Register path="/DefenseRegister/Register/:defenseId" />
            <AdminHome path="/AdminHome" />
            <EditCourse path="/Courses/EditCourse/:courseId" />
            <AddCourse path="/Courses/AddCourse" />
            <ViewUsers path="/ViewUsers" />
            <About path="/About" />
            <Home default />
            <AddDefense path="/Defenses/AddDefense/:courseId" />
            <ShowDefenses path="/Defenses/ShowDefenses/:courseId" />
            <EditDefense path="/Defenses/EditDefense" />
            <Statistics path="/Statistics" />
            <Facebook path="/Facebook" />
          </Router>
          <Location>
            {({ location }) => (
              <Component
                location={location}
                didMount={() => track(location.pathname)}
                didUpdate={({ prevProps }) => {
                  if (prevProps.location !== location) {
                    track(location.pathname);
                  }
                }}
              />
            )}
          </Location>
        </div>
      </ThemeProvider>
    );
  }
}

export default withStyles(styles, { withTheme: true })(App);
