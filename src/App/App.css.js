const drawerWidth = 200;
export const styles = theme => ({
  root: {
    display: "flex",
    direction: "rtl",
    fontFamily: "VarelaRound"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    position: "fixed",
    top: 0
  },
  userInfo: {
    direction: "rtl"
  },
  siteInfo: {
    left: 0,
    position: "fixed",
    marginLeft: "2%"
  },
  linkText: {
    textAlign: "right"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#F9F9F9"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  toolbar: theme.mixins.toolbar
});
